/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import org.dom4j.Attribute;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.DocumentResult;
import org.dom4j.io.DocumentSource;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author ed
 */
public class xmlDom4jParser {

    public static void main(String[] args) throws IOException, DocumentException {

        //Create document
        xmlDom4jParser xml = new xmlDom4jParser();
        //xml.write(xml.createDocument());

        //Read xml
        File f = new File("/home/ed/NetBeansProjects/testing/files/output.xml");
//        URL url = new URL("/home/ed/NetBeansProjects/testing/files/output.xml");
        Document document = xml.parse(f);
        xml.foo(document);
    }

    public void write(Document document) throws IOException {

        File f = new File("/home/ed/NetBeansProjects/testing/files/");
        if (!f.exists()) {
            f.mkdirs();
        }
        // lets write to a file
        XMLWriter writer = new XMLWriter(
                new FileWriter("/home/ed/NetBeansProjects/testing/files/output.xml")
        );
        writer.write(document);
        writer.close();

        // Pretty print the document to System.out
        OutputFormat format = OutputFormat.createPrettyPrint();
        writer = new XMLWriter(System.out, format);
        writer.write(document);

        // Compact format to System.out
        format = OutputFormat.createCompactFormat();
        writer = new XMLWriter(System.out, format);
        writer.write(document);
    }

    public Document createDocument() {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("eZum");

        Element plan = root.addElement("plan")
                .addAttribute("nombre", "Plan EPS")
                .addAttribute("tipo", "Etario")
                .addText("Plan EPS");

        Element beneficio = root.addElement("beneficio")
                .addAttribute("descripcion", "ATENCIÓN AMBULATORIA 1")
                .addAttribute("detalle", "Atenciones ambulatorias relativas de prestaciones de capa simple y/o capa compleja.")
                .addText("Atenciones ambulatorias relativas de prestaciones de capa simple y/o capa compleja.");

        return document;
    }

    public Document parse(File file
    /*        , URL url*/
    ) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        return document;
    }

    public void readXml(Document document) {
        
        System.out.println("Root Name: "+document.getRootElement().getName());
        System.out.println("Document encoding: "+document.getXMLEncoding()); 
        Element classElement = document.getRootElement();
         
//        List list = document.selectNodes("//plan");
//
//        Node node = document.selectSingleNode("/eZum/beneficios");
//
//        String name = node.valueOf("@descripcion");
//        System.out.println(name);
    }

    public void foo(Document document) throws DocumentException {

        Element eZum = document.getRootElement();
        System.out.println("1.." + eZum);
        // iterate through child elements of root
        for (Iterator i = eZum.elementIterator(); i.hasNext();) {
            Element element = (Element) i.next();
            System.out.println(element);
            // do something
        }

        // iterate through child elements of root with element name "foo"
        for (Iterator i = eZum.elementIterator("plan"); i.hasNext();) {
            Element element = (Element) i.next();
            System.out.println("-" + element);
            // do something
        }

        // iterate through attributes of root 
        for (Iterator i = eZum.attributeIterator(); i.hasNext();) {
            Attribute attribute = (Attribute) i.next();
            System.out.println(".." + attribute);
            // do something
        }
    }

    public Document styleDocument(
            Document document,
            String stylesheet
    ) throws Exception {

        // load the transformer using JAXP
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(
                new StreamSource(stylesheet)
        );

        // now lets style the given document
        DocumentSource source = new DocumentSource(document);
        DocumentResult result = new DocumentResult();
        transformer.transform(source, result);

        // return the transformed document
        Document transformedDoc = result.getDocument();
        return transformedDoc;
    }
}
