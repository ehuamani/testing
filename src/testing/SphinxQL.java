/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.sql.*;

/**
 *
 * @author ed
 */
public class SphinxQL {

//    public static Connection getConnection() throws SQLException {
//        return DriverManager.getConnection("jdbc:mysql://127.0.0.1:9306?characterEncoding=utf8&maxAllowedPacket=512000", "root", "123");
//    }
    public static Connection getConnection() {

        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/zumfinal", "postgres",
                    "1234");

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }
        return connection;
    }

    public static void executeQuery(String query) throws SQLException {
        Connection aConnection = getConnection();
        Statement aStatement;
        aStatement = aConnection.createStatement();
        ResultSet aResultSet = aStatement.executeQuery(query);
        while (aResultSet.next()) {
            System.out.println(aResultSet.getString(1));
        }
        aConnection.close();
        aStatement.close();
        aResultSet.close();
    }

    public static void main(String[] argv) throws Exception {
        Class.forName("org.postgresql.Driver");
        executeQuery("SELECT * FROM t_user WHERE MATCH('a')");
    }

}
