package testing;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.codec.binary.Base64;

/**
 * Test cifrar datos
 *
 * @author: Edgar Huamani A.
 * @version: 02/02/2014/v1
 * @see <a href = "http://www.unasolutions.com" /> unasolutions.com – Desarrollo
 * de software </a>
 */
public class X509CertEncrypt {

    private final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";
    private final String KEYP12PATHNAME = "/home/ed/k/certificate/zumKeys/kzum.p12";
    private final String KEYSTORE_PASSWORD = "zumseguros.com";
    private final String PRIVATE_KEY_PASSWORD = "zumseguros.com";
    private final String KEYSTORE_KEY_ALIAS = "1";
    private final String KEYSTORE_TYPE = "PKCS12";

    public void test() throws Exception {
        /**
         * Cargamos data en Lista
         */
        List<networkWs> lnetworkWs = new ArrayList<>();
        networkWs network = new networkWs();
        network.setNetwVidref("001");
        network.setNetwVdescr("ATENCIÓN AMBULATORIA BÁSICA");
        network.setNetwCstatu("A");
        lnetworkWs.add(network);
        network = new networkWs();
        network.setNetwVidref("002");
        network.setNetwVdescr("ATENCIÓN AMBULATORIA - MEDICINA GENERAL");
        network.setNetwCstatu("A");
        lnetworkWs.add(network);

        Type type = new TypeToken<List<networkWs>>() {}.getType();
        JsonObject oNetwork = new JsonObject();
        oNetwork.addProperty("networkWs", new Gson().toJson(lnetworkWs,type));
        //Cadena cifrada
        String data = encrypt(oNetwork);
        System.out.println("\n");
        System.out.println("Objeto antes de cifrar: " + new Gson().toJson(oNetwork));
        System.out.println("\n");
        System.out.println("Mensaje cifrado: " + data);
        System.out.println("\n");
        System.out.println("Cadena luego de descifrar: " + decrypt(data));

    }

    private KeyStore.PrivateKeyEntry getKeyEntry() throws Exception {
        KeyStore clientKeyStore = KeyStore.getInstance(KEYSTORE_TYPE);
        InputStream is = new FileInputStream(new File(KEYP12PATHNAME));
        clientKeyStore.load(is, KEYSTORE_PASSWORD.toCharArray());
        KeyStore.PasswordProtection keyPassword = new KeyStore.PasswordProtection(
                PRIVATE_KEY_PASSWORD.toCharArray());
        KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) clientKeyStore.getEntry(KEYSTORE_KEY_ALIAS, keyPassword);
        if (!javax.security.cert.X509Certificate.getInstance(pkEntry.getCertificate().getEncoded())
                .getNotAfter().after(new Date())) {
            throw new Exception("El certificado ha expirado");
        }
        return pkEntry;
    }

    private String encrypt(JsonObject oNetwork) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, getKeyEntry().getCertificate().getPublicKey());
        Gson gson = new Gson();
        String json = gson.toJson(oNetwork);
        byte[] data = cipher.doFinal(json.getBytes());
        return Base64.encodeBase64String(data);
    }

    private String decrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, InvalidKeySpecException, InvalidKeyException, CertificateException, KeyStoreException, NoSuchProviderException, UnrecoverableKeyException, Exception {
        Cipher decryptCipher = Cipher.getInstance(TRANSFORMATION);
        decryptCipher.init(Cipher.DECRYPT_MODE, getKeyEntry().getPrivateKey());
        byte[] data = Base64.decodeBase64(str);
        byte[] messageDecrypte = decryptCipher.doFinal(data);
        return new String(messageDecrypte);
    }

    /**
     * @param args the command line arguments
     */
    public class networkWs {

        private String netwVidref;
        private String netwVdescr;
        private String netwCstatu;

        public String getNetwVidref() {
            return netwVidref;
        }

        public void setNetwVidref(String netwVidref) {
            this.netwVidref = netwVidref;
        }

        public String getNetwVdescr() {
            return netwVdescr;
        }

        public void setNetwVdescr(String netwVdescr) {
            this.netwVdescr = netwVdescr;
        }

        public String getNetwCstatu() {
            return netwCstatu;
        }

        public void setNetwCstatu(String netwCstatu) {
            this.netwCstatu = netwCstatu;
        }

        @Override
        public String toString() {
            return "{" + "id=" + netwVidref + ", name=" + netwVdescr + ", address=" + netwCstatu + '}';
        }
    }

    public static void main(String[] args) {
        X509CertEncrypt oTesting = new X509CertEncrypt();
        try {
            oTesting.test();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
