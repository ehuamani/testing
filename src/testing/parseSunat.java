/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author ed
 */
public class parseSunat {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        BufferedReader br = null;
        Connect conn = new Connect();
        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader("/home/ed/Documentos/projects/sunat-ws/padron_reducido_ruc.txt"));
            int i = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                if (i > 0) {
                    String data[] = sCurrentLine.split("\\|");
                    conn.setQuery(data);
                }

                i++;
                if (i >= 10000) {
                    return;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static class Connect {

        private String url = "jdbc:mysql://localhost:3306/sunat";
        private String user = "root";
        private String password = "123";
        private String driver = "com.mysql.jdbc.Driver";
        private Connection conn;

        public Connect() throws SQLException, ClassNotFoundException {
            Class.forName(driver);
            this.conn = DriverManager.getConnection(url, user, password);
        }

        public Connection getConn() {
            return conn;
        }

        public void setQuery(String[] data) throws SQLException {
            String ruc = data[0];
            String nombre_razon_social = data[1];
            String estado_contribuyente = data[2];
            String condicion_domicilio = data[3];
            String ubigeo = data[4];
            String tipo_via = data[5];
            String nombre_via = data[6];
            String codigo_zona = data[7];
            String tipo_zona = data[8];
            String numero = data[9];
            String interior = data[10];
            String lote = data[11];
            String departamento = data[12];
            String manzana = data[13];
            String kilometro = data[14];
            String query = " insert into padron_reducido_ruc (ruc,"
                    + " nombre_razon_social,"
                    + " estado_contribuyente,"
                    + " condicion_domicilio,"
                    + " ubigeo, tipo_via,"
                    + " nombre_via,"
                    + " codigo_zona,"
                    + " tipo_zona,"
                    + " numero,"
                    + " interior,"
                    + " lote,"
                    + " departamento,"
                    + " manzana,"
                    + " kilometro)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = this.conn.prepareStatement(query);
            preparedStmt.setString(1, data[0]);
            preparedStmt.setString(2, data[1]);
            preparedStmt.setString(3, data[2]);
            preparedStmt.setString(4, data[3]);
            preparedStmt.setString(5, data[4]);
            preparedStmt.setString(6, data[5]);
            preparedStmt.setString(7, data[6]);
            preparedStmt.setString(8, data[7]);
            preparedStmt.setString(9, data[8]);
            preparedStmt.setString(10, data[9]);
            preparedStmt.setString(11, data[10]);
            preparedStmt.setString(12, data[11]);
            preparedStmt.setString(13, data[12]);
            preparedStmt.setString(14, data[13]);
            preparedStmt.setString(15, data[14]);

//                    System.out.println("1->"+preparedStmt);
            // execute the preparedstatement
            preparedStmt.execute();
//
            conn.close();
        }

    }

}
