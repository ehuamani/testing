package testing;

import java.awt.Point;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.Random;
import org.postgresql.geometric.PGpoint;

public class test {

    public static void main(String[] args) throws SQLException {
        Connection conn = getConnection();
        PreparedStatement st = conn.prepareStatement("SELECT user_id, user_gp FROM t_user;");
        ResultSet result = st.executeQuery();

        while (result.next()) {
//            System.out.println("id: " + result.getString("user_id") + " | " + result.getString("user_gp"));
        }

    }

    public static void insertCode() throws SQLException {
        Connection conn = getConnection();
        PGpoint gp = null;
        PreparedStatement st;

        for (int i = 0; i < 86; i++) {
            Double n = -12.1188287;
            Double w = -77.0279879;
            n = randomInRange(1188237.0, 1188287.0);
            n = Double.parseDouble("-12." + n.intValue());
            gp = new PGpoint(n, w);
            st = conn.prepareStatement("INSERT INTO \"t_user\" (\"user_gp\") VALUES (?);");
            st.setObject(1, gp);
            st.execute();
            st.close();
        }
    }

    protected static Random random = new Random();

    public static double randomInRange(double lower, double upper) {
        double start = lower;
        double end = upper;
        double random = new Random().nextDouble();
        double result = start + (random * (end - start));
        return Math.round(result);
    }

    public static Connection getConnection() {

        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

//        System.out.println("PostgreSQL JDBC Driver Registered!");
        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/testing", "postgres",
                    "1234");

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

//        if (connection != null) {
//            System.out.println("You made it, take control your database now!");
//        } else {
//            System.out.println("Failed to make connection!");
//        }
        return connection;
    }
}
