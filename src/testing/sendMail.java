/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author ed
 */
public class sendMail {

//    private static String username = "zumseguros@gmail.com";
//    private static String password = "Zumseguros2015$!";

    public static void main(String[] args) throws IOException {
        sendmailToken("ehuamani@unasolutions.com");
    }

    public static boolean sendmailToken(String destino) throws IOException {
        boolean result = false;
        String asunto = "Test";
        String host = "localhost";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ehuamani@unasolutions.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(destino));
            message.setSubject(asunto);
//            String html = "";
//            html = "/var/www/una/mini-cooper/4930514e-dad8-465d-9e68-04acf4c0b80a.html";
            
            File input = new File("/var/www/una/mini-cooper/4930514e-dad8-465d-9e68-04acf4c0b80a.html");
            Document document = Jsoup.parse(input, "UTF-8", "http://example.com/");
            String html = document.toString();
            
            Multipart multipart = new MimeMultipart();
            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(html, "text/html");
            multipart.addBodyPart(htmlPart);
            message.setContent(multipart);
            Transport.send(message);
            System.out.println("Done");
            result = true;

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return result;

    }
}
