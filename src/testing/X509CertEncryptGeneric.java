/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

/**
 *
 * @author ed
 */
public class X509CertEncryptGeneric {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(512);
        KeyPair rsaKeyPair = kpg.genKeyPair();
        byte[] txt = "Esto es un mensaje.".getBytes();
        System.out.println("Original clear message: " + new String(txt));

        // encrypt
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, rsaKeyPair.getPublic());
            txt = cipher.doFinal(txt);
        } catch (Throwable e) {
            e.printStackTrace();
            return;
        }
        System.out.println("Encrypted message: " + new String(txt));

        // decrypt
        try {
            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, rsaKeyPair.getPrivate());
            txt = cipher.doFinal(txt);
        } catch (Throwable e) {
            e.printStackTrace();
            return;
        }
        System.out.println("Decrypted message: " + new String(txt));
    }
}
