/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ed
 */
public class createqueryzum {

    private void run() {
        String strBenefits
                = "ATENCIÓN AMBULATORIA,"
                + "ATENCIÓN OFTALMOLÓGICA,"
                + "ATENCIÓN HOSPITALARIA,"
                + "RIESGO QUIRURGICO,"
                + "PROGRAMA DE MATERNIDAD,"
                + "ATENCIÓN DE EMERGENCIAS,"
                + "TERAPIA FÍSICA Y REHABILITACIÓN,"
                + "PRÓTESIS QUIRÚRGICAS INTERNAS,"
                + "ONCOLOGÍA,"
                + "BENEFICIOS DE SALUD MENTAL,"
                + "ATENCIÓN PREVENTIVO PROMOCIONALES,"
                + "PREPARACIÓN PARA EL PARTO (PSICOPROFILAXIS),"
                + "ENFERMEDADES CONGÉNITAS,"
                + "COBERTURA CON TERAPIA BIOLOGICA PARA CASOS NO ONCOLÓGICOS,"
                + "BOTOX (TOXINA BOTULÍNICA) PARA CASOS TERAPÉUTICOS,"
                + "TERAPIA HORMONAL PARA MENOPAUSIA,"
                + "DESASTRES NATURALES,"
                + "TERRORISMO,"
                + "ENFERMEDADES EPIDÉMICAS,"
                + "GASTOS POR TRASPLANTE DE ÓRGANOS (Sólo al crédito),"
                + "PLANIFICACIÓN FAMILIAR (INSERCIÓN  Y RETIRO DEL DISPOSITIVO INTRAUTERINO)";

        String strNetwork
                = "Red 1,"
                + "Provincias A,"
                + "Red 2,"
                + "Provincias B,"
                + "Red 3,"
                + "Red 4,"
                + "Red 5,"
                + "Red 6,"
                + "Red Propia CENTRO SALUD PACIFICO,"
                + "Red Propia CUIDADO CONTINUO DE LA SALUD,"
                + "Pacífico Asiste MEDICOS A DOMICILIO Lima 1,"
                + "Pacífico Asiste MEDICOS A DOMICILIO  Lima 2,"
                + "Pacífico Asiste MEDICOS A DOMICILIO Provincias,"
                + "PROGRAMA NUTRICIONAL,"
                + "Red Odontológica 1 (deducible por pieza tratada y terminada),"
                + "Red Oftalmológica 1,"
                + "Red Oftalmológica 2,"
                + "Red 1,"
                + "Provincias A,"
                + "Red 2,"
                + "Provincias B,"
                + "Red 3,"
                + "Red 4,"
                + "Red 5,"
                + "Red 6,"
                + "Red 1,"
                + "Provincias A,"
                + "Red 2,"
                + "Provincias B,"
                + "Red 3,"
                + "Red 4,"
                + "Red 5,"
                + "Red 6,"
                + "Red 1,"
                + "Provincias A,"
                + "Red 2,"
                + "Provincias B,"
                + "Red 3,"
                + "Red 4,"
                + "Red 5,"
                + "Red 6,"
                + "Red 1,"
                + "Provincias A,"
                + "Red 2,"
                + "Provincias B,"
                + "Red 3,"
                + "Red 4,"
                + "Red 5,"
                + "Red 6,"
                + "Continuidad de una Emergencia Accidental Ambulatoria,"
                + "Emergencias Graves a Domicilio,"
                + "Transporte por  Evacuación,"
                + "Consulta médica especializada,"
                + "Terapia física,"
                + "Continuidad de una Emergencia Accidental Ambulatoria,"
                + "Crédito,"
                + "Unidad de Consejería Oncológica (UCO),"
                + "Atención oncológica ambulatoria,"
                + "Atención oncológica hospitalaria,"
                + "Apoyo al diagnóstico - Imágenes,"
                + "Petscan,"
                + "Radioterapia,"
                + "Tratamiento de última generación con modificadores de respuesta biológica para el Cáncer,"
                + "Otros beneficios,"
                + "Oncoayuda,"
                + "Red Psicológica 1,"
                + "Red Psicológica 1,"
                + "Red Psicológica2,"
                + "Red Psicológica 1,"
                + "Red Psicológica 1,"
                + "Red psicológica 2 ,"
                + "Red Psicológica 1,"
                + "Red psicológica 2,"
                + "Red Psicológica 1,"
                + "Chequeos preventivos,"
                + "Inmunizaciones,"
                + "Crédito,"
                + "Tratamiento de última generación con modificadores de respuesta biológica,"
                + "Para pertinencia médica con fines terapéuticos";
        String strSucursal
                = "Clínica El Golf,"
                + "Centro Médico San Judas Tadeo,"
                + "Clínica Limatambo San Isidro,"
                + "Clínica Limatambo San Juan De Lurigancho,"
                + "Clínica Montefiori,"
                + "Clínica Santa María Del Sur,"
                + "Clínica Good Hope,"
                + "Clínica Nuevo San Juan,"
                + "Clínica Vesalio,"
                + "Clínica Limatambo Callao,"
                + "Clínica Mundo Salud,"
                + "Clínica La Luz,"
                + "Clínica San Borja,"
                + "Centro Salud Pacífico (Talara),"
                + "Centro Salud Pacífico (Negritos),"
                + "Clínica Belén (Piura),"
                + "Centro Medico Pediátrico Carita Feliz (Piura),"
                + "Hospital Metropolitano (María Izaga Chiclayo),"
                + "Clínica Sánchez Ferrer (Trujillo),"
                + "Clínica Limatambo Cajamarca,"
                + "Clínica Los Fresnos (Cajamarca),"
                + "C.Monte Carmelo (Arequipa),"
                + "Hogar Clínica San Juan De Dios (Arequipa),"
                + "Clínica Ortega (Huancayo),"
                + "Hospital Privado Juan Pablo II (Chiclayo),"
                + "Clínica Galeno (Arequipa),"
                + "Hospital Nivel II Essalud Alberto Hurtado Abadia (La Oroya),"
                + "Centro Clínico Sanna (Chiclayo),"
                + "Clínica Los Condes (Ica),"
                + "Centro Medico Jockey Salud,"
                + "Centro Médico Ricardo Palma Plaza Lima Sur,"
                + "Clínica Centenario Peruano Japonesa,"
                + "Clínica Stella Maris,"
                + "Clínica Ricardo Palma (Sede Norte),"
                + "Clínica San Gabriel,"
                + "Clínica Bellavista,"
                + "Centro Médico Medex,"
                + "Clínica Médica Cayetano Heredia.,"
                + "Clínica Providencia,"
                + "Centro Medico Especializado de Medicina Avanzada - MEDAVAN (*),"
                + "Clínica Internacional (Medicentro El Polo),"
                + "Clínica Internacional (Medicentro San Borja),"
                + "Clínica Internacional (Medicentro San Isidro),"
                + "Clínica Internacional Sede San Borja,"
                + "Clínica Internacional Sede Lima,"
                + "Centro Salud Pacifico (Cajamarca),"
                + "Clínica Peruano Americana (Trujillo),"
                + "Clínica Famisalud (Pisco),"
                + "Clínica San Miguel (Piura),"
                + "Clínica Adventista Ana Stahl (iquitos),"
                + "Clínica Arequipa (Arequipa),"
                + "Clínica Padre Luis Tezza,"
                + "Clínica Especialidades Medicas Universal,"
                + "Clínica Jesús Del Norte,"
                + "Clínica Maison De Sante - Chorrillos,"
                + "Clínica Maison De Sante - Lima,"
                + "Urología de Avanzada (*),"
                + "Centro Medico Especializado Avendaño (*),"
                + "Clínica Javier Prado,"
                + "Clínica Novocardio (*),"
                + "Clínica Ricardo Palma (Sede Central),"
                + "Clínica San Pablo (Sede Central),"
                + "Clínica Santa Isabel,"
                + "Clínica British American Hospital,"
                + "Clínica Montesur,"
                + "Clínica San Felipe,"
                + "Clínica El Golf,"
                + "Clínica Nuevo San Juan,"
                + "Clínica Limatambo San Isidro,"
                + "Clínica Limatambo San Juan De Lurigancho,"
                + "Clínica Montefiori,"
                + "Clínica Good Hope,"
                + "Clínica Vesalio,"
                + "Clínica Mundo Salud,"
                + "Clínica La Luz,"
                + "Clínica San Borja,"
                + "Clínica Galeno (Arequipa),"
                + "Hospital Privado Juan Pablo II (Chiclayo),"
                + "Hospital Metropolitano (María Izaga Chiclayo),"
                + "Clínica Sánchez Ferrer (Trujillo),"
                + "Clínica Limatambo Cajamarca,"
                + "Clínica Los Fresnos (Cajamarca),"
                + "C.Monte Carmelo (Arequipa),"
                + "Clínica Ortega (Huancayo),"
                + "Clínica Belén (Piura),"
                + "Hospital Nivel II Essalud Alberto Hurtado Abadia (La Oroya),"
                + "Clínica Los Condes (Ica),"
                + "Centro Medico Jockey Salud,"
                + "Clínica Stella Maris,"
                + "Clínica Centenario Peruano Japonesa,"
                + "Clínica San Gabriel,"
                + "Clínica Bellavista ,"
                + "Clínica Médica Cayetano Heredia.,"
                + "Clínica Providencia,"
                + "Clínica Internacional Sede Lima,"
                + "Clínica Internacional Sede San Borja,"
                + "Centro Salud Pacifico (Cajamarca),"
                + "Clínica Peruano Americana (Trujillo),"
                + "Clínica Famisalud (Pisco),"
                + "Clínica San Miguel (Piura),		Clínica Adventista Ana Stahl (iquitos), 		Clínica Arequipa (Arequipa),"
                + "Clínica San Borja,		Clínica Especialidades Medicas Universal,		Clínica Jesús Del Norte,"
                + "Clínica Maison De Sante - Chorrillos		,Clínica Maison De Sante - Lima		,Clínica Padre Luis Tezza,"
                + "Clínica Javier Prado				,"
                + "Clínica Ricardo Palma (Sede Central)	,	Clínica San Pablo (Sede Central)		,Concebir"
                + "Clínica Santa Isabel				,"
                + "Clínica British American Hospital		,Clínica Montesur		,Clínica San Felipe ,"
                + "Clínica El Golf		,Clínica Nuevo San Juan		,Clínica Limatambo San Isidro"
                + "Clínica Limatambo San Juan De Lurigancho		,Clínica Montefiori		,Clínica Good Hope"
                + "Clínica Vesalio		,Clínica Mundo Salud 		,Clínica La Luz "
                + "Clínica San Borja		,		"
                + "Clínica Galeno (Arequipa)	,	Hospital Privado Juan Pablo II (Chiclayo)	,	Hospital Metropolitano (María Izaga Chiclayo)"
                + "Clínica Sánchez Ferrer (Trujillo)	,	Clínica Limatambo Cajamarca,		Clínica Los Fresnos (Cajamarca)"
                + "C.Monte Carmelo (Arequipa)	,	Clínica Ortega (Huancayo), 		Clínica Belén (Piura)"
                + "Hospital Nivel II Essalud Alberto Hurtado Abadia (La Oroya)	,	Clínica Los Condes (Ica)		,"
                + "Clínica San Gabriel	,	Clínica Stella Maris,		Clínica Centenario Peruano Japonesa,"
                + "Clínica Bellavista 	,	Clínica Médica Cayetano Heredia	,	Clínica Providencia,"
                + "Clínica Internacional Sede Lima	,	Clínica Internacional Sede San Borja	,"
                + "Clínica Arequipa (Arequipa)	,	Clínica Peruano Americana (Trujillo)	,	Clínica Famisalud (Pisco)"
                + "Clínica San Miguel (Piura)	,	Clínica Adventista Ana Stahl (iquitos) 	,	"
                + "Clínica Padre Luis Tezza	,	Clínica Especialidades Medicas Universal	,	Clínica Jesús Del Norte,"
                + "Clínica Maison De Sante - Chorrillos	,	Clínica Maison De Sante - Lima		,"
                + "Clínica Javier Prado			,"
                + "Clínica Ricardo Palma (Sede Central), 		Clínica San Pablo (Sede Central), 		Clínica Santa Isabel, \n"
                + "Concebir				,"
                + "Clínica British American Hospital 	,	Clínica Montesur	,	Clínica San Felipe ,"
                + "Instituto Oncológico Miraflores		,Clínicas afiliadas a su plan de salud.,"
                + "DPI del Perú		,Cerema		,Emetac,"
                + "Resomasa		,Resocentro		,Graciela Citera (Medicina Nuclear)"
                + "Cimedic 		,Clínicas afiliadas a su plan de salud.		,"
                + "Centro de diagnóstico Pet CT Perú.				,"
                + "Centro de Radioterapia de Lima		,Clínica Vesalio		,Centro Oncológico  de Chiclayo	,"
                + "Clínica Good Hope 	,	Clínica Nuevo San Juan	,	Clinica Limatambo San Isidro,"
                + "Clínica Stella Maris	,	Clínica San Gabriel	,	Escuela para Embarazadas,"
                + "Clínica Centenario Peruano Japonesa 	,	Clínica Jesús del Norte	,	Servicios Prenatal,"
                + "Clínica San Pablo Surco	,	Clínica Ricardo Palma sede central	,	Clínica Internacional Sede San Borja,"
                + "";

        String[] strBenefitList = strBenefits.split(",");
        String[] strNetworkList = strNetwork.split(",");
        String[] strSucursalList = strSucursal.split(",");
        List<String> strList = new ArrayList<>();

        String query = "";

        System.out.println("\t"+type);
        System.out.println("----------------\n");
        switch (type){
            case '1':
                for (String s : strBenefitList) //Load Benefit List
                    if(!isContrain(s, strList))
                        strList.add(s);
                
                query = "INSERT INTO t_benefit (bene_iident, bene_vdescr, bene_vderes, bene_dfecre, bene_cstatu, bene_cfrecu, ense_iident, bene_vcodre, bene_vitref) VALUES (nextval('sq_benefit'), 'rep1', 'rep2', now(), 'A', '1', '100', NULL, NULL);";
                break;
            case '2':
                for (String s : strNetworkList) //Load Network List
                    if(!isContrain(s, strList))
                        strList.add(s);
                query = "INSERT INTO t_network (netw_iident, netw_vdescr, netw_cstatu, nety_iident, ense_iident, netw_vidref, netw_vitref) VALUES (nextval('sq_network'), 'rep1', 'A', '1', '100', NULL, NULL);";
                break;
            case '3':
                for (String s : strSucursalList) //Load Network List
                            if (!isContrain(s, strList))
                                strList.add(s);
                query = "INSERT INTO t_sucursal_provider (sucu_iident, prov_iident, sucu_vaddre, sucu_vdescr, loca_iident, sucu_cstatu, ense_iident, sucu_vidref, sucu_vitref, sucu_vipref, sucu_vgmapu) VALUES (nextval('sq_sucursalprovider'), '41', 'lima', 'rep1', '1471', 'A', '100', NULL, NULL, NULL, NULL);";
                break;
               
        }
        for (String s : strList) {
//            if(s.length() > 80);
//            System.err.println(query.replace("rep1", s.trim()).replace("rep2", s.trim()));
                System.out.println(query.replace("rep1", s.trim()).replace("rep2", s.trim()));

        }
    }

    private final Character type;

    public createqueryzum(Character ptype) {
        type = ptype;

    }

    public static void main(String[] args) {
        createqueryzum t = new createqueryzum('1');
        t.run();
    }

    private Boolean isContrain(String s, List<String> strL) {
        for (String str : strL) {
            if (s.equals(str)) {
                return true;
            }
        }
        return false;
    }
}
