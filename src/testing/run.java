/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ed
 */
public class run {

    public static void main(String[] args) throws FileNotFoundException {
//        String json = "{\"token\":\"20150288dc5b7b5869674b06bb76b58d3d264d81\",\"document\":\"dca3437962e79ef1cd1b46f33f46fe92467c75f49a234e39a6d2a6140e051cbe\",\"typedocument\":\"5400a528d649a0b923a6d0a2b3403381c09d57f23f5d9cc43184bf746558fc9a\",\"typeentity\":\"C\"}";
//        JsonObject jData = new Gson().fromJson(json, JsonObject.class);
//         System.out.println("-->"+jData);
//        String typeentity = jData.get("typeentity").getAsString();
//        String document = jData.get("document").getAsString();
//        String typedocument = jData.get("typedocument").getAsString();
//        String token = jData.get("token").getAsString();
//        
//        String i = UtilZum.encrypttext("23165456465");
//        String o = UtilZum.decrypttext(i);
//        System.out.println("--> i: "+i + " | o: "+o);

//        System.out.println("typeentity :" +typeentity+
//"        document: " +UtilZum.decrypttext(document)+
//"        typedocument :" +UtilZum.decrypttext(typedocument)+
//"        token: "+token);
        System.out.println(UtilZum.decrypttext("f3c5ba6cd298eff7445e5fb7f0e4b881413889baf46dc9910a2e33c39f99acdc"));
//        System.out.println(UtilZum.encrypttext("35"));
    }

    public String query() throws FileNotFoundException {
        return "";
    }

    public JsonArray queryList() throws FileNotFoundException {
        File file = new File(UtilZum.getProjectPath() + "util/query.json");
        if (!file.isFile()) {
            System.err.println("ERROR: El archivo: " + file.getAbsolutePath() + " No se encuentra disponible");
            return null;
        }
        BufferedReader br = new BufferedReader(new FileReader(file));
        Gson gson = new Gson();
        JsonElement elementJson = gson.fromJson(br, JsonElement.class);
        if (elementJson.isJsonNull()) {
            System.err.println("ERROR: structura de configuación inicial no está configurada correctamente");
            return null;
        }
        JsonObject jsonObject = elementJson.getAsJsonObject();
        if (jsonObject == null) {
            System.err.println("ERROR: Estructura de configuración incorrecta");
            return null;
        }
        jsonObject = jsonObject.getAsJsonObject("150");
        if (jsonObject == null) {
            System.err.println("ERROR: Configuración para el TR '" + "150" + "' No encontrado");
            return null;
        }
        JsonArray arraySearch = jsonObject.getAsJsonArray("SEARCH");
        if (arraySearch == null) {
            System.err.println("ERROR: Elementos de búsqueda no encontrados");
            return null;
        }
        return arraySearch;
    }

    public void differenceDate() {
        /* Config */
        Boolean defaultDate = true;

        String dateStart = "01/09/2015 17:15:30";
        String dateStop = "01/09/2015 17:08:48";

//        dateStart= 2;
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);

            if (defaultDate) {
                d2 = new Date();
            } else {
                d2 = format.parse(dateStop);
            }

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
