package testing;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.FileSystems;

import java.security.*; 
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;

import javax.crypto.*;  
import javax.crypto.spec.IvParameterSpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

public class UtilZum {

	protected static Logger logger = Logger.getLogger(UtilZum.class);
        
	public static String dateToString(Date Fecha){
			String result="";
			if(Fecha!=null){
				DateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy");
				result = fechaHora.format(Fecha);
			}
			return result;
	}
	public static String nulltoString(Object valor){
		String result="";
		if(valor!=null){
			result=(String) valor;
		}
		return result;
	}
	public static String dateToStringyear(Date Fecha){
		String result="";
		DateFormat fechaHora = new SimpleDateFormat("yyyy");
		result = fechaHora.format(Fecha);
		return result;
	}
	
    public static Date getPrimerDiaDelMes() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), 
                cal.get(Calendar.MONTH),
                cal.getActualMinimum(Calendar.DAY_OF_MONTH),
                cal.getMinimum(Calendar.HOUR_OF_DAY),
                cal.getMinimum(Calendar.MINUTE),
                cal.getMinimum(Calendar.SECOND));
        return cal.getTime();
    }

    public static Date getUltimoDiaDelMes() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                cal.getMaximum(Calendar.HOUR_OF_DAY),
                cal.getMaximum(Calendar.MINUTE),
                cal.getMaximum(Calendar.SECOND));
        return cal.getTime();
    }
    
    public static int diferenciaFechasEnDias(Date fechaInicial, Date fechaFinal) {
    	
        if(fechaInicial==null || fechaFinal==null){
        	return 0;
        }
    	DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    	String fechaInicioString = df.format(fechaInicial);
    	try {
    		fechaInicial = df.parse(fechaInicioString);
    	}
    	catch (ParseException ex) {
    	}

    	String fechaFinalString = df.format(fechaFinal);
    	try {
    		fechaFinal = df.parse(fechaFinalString);
    	}
    	catch (ParseException ex) {
    	}

    	long fechaInicialMs = fechaInicial.getTime();
    	long fechaFinalMs = fechaFinal.getTime();
    	long diferencia = fechaFinalMs - fechaInicialMs;
    	double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
    	return ( (int) dias);
    }

	
	private static final DecimalFormat timeFormat4 = new DecimalFormat("0000;0000");

	public static String getkeytoken() {
	    Calendar cal = Calendar.getInstance();
	    String val = String.valueOf(cal.get(Calendar.YEAR));
	    val += timeFormat4.format(cal.get(Calendar.DAY_OF_YEAR));
	    val += UUID.randomUUID().toString().replaceAll("-", "");
	    return val;
	}
	
	@SuppressWarnings("rawtypes")
	private static void Leer(List cellDataList) throws Exception{
		try {
			  String campo1="";
        	  String campo2="";
			for (int i = 1; i < cellDataList.size(); i++){
				List cellTempList = (List) cellDataList.get(i);
				for (int j = 0; j < cellTempList.size(); j++){
					XSSFCell hssfCell = (XSSFCell) cellTempList.get(j);
					System.out.println("sdsff"+hssfCell.toString());
					
					if(j==0 ){
						campo1=hssfCell.toString().trim();
					}
					if(j==1 ){
						campo2=hssfCell.toString().trim();
					}					
				}
				System.out.println("codiog: "+campo1 +"\t descr:"+campo2);

			 }
             System.out.println("\nPROCESO CORRECTO");
             
       }catch (Exception e){ //Catch de excepciones
           System.out.println("OCURRIO ERROR : " + e.getMessage());
       }finally{
       
       }
		
	}
	
	
	

	@SuppressWarnings("unchecked")
	public static void pleerexcel3(InputStream fileInputStream) throws Exception{
		   logger.info("pleerexcel2");

				@SuppressWarnings("rawtypes")
				List cellDataList = new ArrayList();
				try{
					XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
					XSSFSheet hssfSheet = workBook.getSheetAt(0);//obteniendo la primera hoja
					@SuppressWarnings("rawtypes")
					Iterator rowIterator = (Iterator) hssfSheet.rowIterator();//creando un itator para traer los datos
						while (rowIterator.hasNext()){
							XSSFRow hssfRow = (XSSFRow) rowIterator.next();
							@SuppressWarnings("rawtypes")
							Iterator iterator = hssfRow.cellIterator();
							@SuppressWarnings("rawtypes")
							List cellTempList = new ArrayList();
							while (iterator.hasNext()){
							XSSFCell hssfCell = (XSSFCell) iterator.next();
							cellTempList.add(hssfCell);
						    }
							cellDataList.add(cellTempList);
					}
				}catch (Exception e)
				{e.printStackTrace();
				}
				Leer(cellDataList);

			}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List pleerexcel2(InputStream fileInputStream) throws Exception{
   logger.info("pleerexcel2");

		List cellDataList = new ArrayList();
		try{
			XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
			XSSFSheet hssfSheet = workBook.getSheetAt(0);//obteniendo la primera hoja
			Iterator rowIterator = (Iterator) hssfSheet.rowIterator();//creando un itator para traer los datos
				while (rowIterator.hasNext()){
					XSSFRow hssfRow = (XSSFRow) rowIterator.next();
					Iterator iterator = hssfRow.cellIterator();
					List cellTempList = new ArrayList();
					
					while (iterator.hasNext()){
						
					XSSFCell hssfCell = (XSSFCell) iterator.next();
					cellTempList.add(hssfCell);
					
				    }
					cellDataList.add(cellTempList);
			}
		}catch (Exception e)
		{e.printStackTrace();
		}
		return cellDataList;
	}
	
	
	@SuppressWarnings("unchecked")
	public static void pleerexcel(String ruta) throws Exception{
		if(ruta.equalsIgnoreCase("")){
		}
		//ruta="D:\\una\\proyectos\\zum\\plantillaexcel\\tp_regist.xlsx";	
		@SuppressWarnings("rawtypes")
		List cellDataList = new ArrayList();
		try{
			FileInputStream fileInputStream = new FileInputStream(ruta);
			XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
			XSSFSheet hssfSheet = workBook.getSheetAt(0);//obteniendo la primera hoja
			@SuppressWarnings("rawtypes")
			Iterator rowIterator = (Iterator) hssfSheet.rowIterator();//creando un itator para traer los datos
				while (rowIterator.hasNext()){
					XSSFRow hssfRow = (XSSFRow) rowIterator.next();
					@SuppressWarnings("rawtypes")
					Iterator iterator = hssfRow.cellIterator();
					@SuppressWarnings("rawtypes")
					List cellTempList = new ArrayList();
					while (iterator.hasNext()){
					XSSFCell hssfCell = (XSSFCell) iterator.next();
					cellTempList.add(hssfCell);
				    }
					cellDataList.add(cellTempList);
			}
		}catch (Exception e)
		{e.printStackTrace();
		}
		Leer(cellDataList);
	}
	
     

	 public static String encodeURIComponent(String s)
	  {
	    String result = null;

	    try
	    {
	      result = URLEncoder.encode(s, "UTF-8")
	                         .replaceAll("\\+", "%20")
	                         .replaceAll("\\%21", "!")
	                         .replaceAll("\\%27", "'")
	                         .replaceAll("\\%28", "(")
	                         .replaceAll("\\%29", ")")
	                         .replaceAll("\\%7E", "~");
	    }

	    // This exception should never occur.
	    catch (UnsupportedEncodingException e)
	    {
	      result = s;
	    }

	    return result;
	  } 

	 
	 public static String decodeURIComponent(String s)
	  {
	    if (s == null)
	    {
	      return null;
	    }

	    String result = null;

	    try
	    {
	      result = URLDecoder.decode(s, "UTF-8");
	    }
	    catch (UnsupportedEncodingException e)
	    {
	      result = s;  
	    }

	    return result;
	  }

	
	public static String dateToStringMilliseconds(){
		String result="";		
		DateFormat fechaHora = new SimpleDateFormat("yyyyMMddhhmmssS");
		result = fechaHora.format(new Date());		
		return result;
	}
	
	 
	public static  String encrypt( String message) {
		String result="";
		try {
			  Key key=getkeyaesfile();
		      IvParameterSpec iv=getivaes();
		      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		      cipher.init(Cipher.ENCRYPT_MODE,key,iv);
		      byte[] stringBytes = message.getBytes();
		      byte[] raw = cipher.doFinal(stringBytes);
		      result= Base64.encodeBase64String(raw);
		} catch (Exception e) {
			 System.out.println("encrypt : " + e.toString());
		}
		return result;
	}

	public static String decrypt( String encrypted) {
		String result="";
		try {
		  Key key=getkeyaesfile();
	      IvParameterSpec iv=getivaes();
	      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	      cipher.init(Cipher.DECRYPT_MODE, key,iv);
	      byte[] raw = Base64.decodeBase64(encrypted);
	      byte[] stringBytes = cipher.doFinal(raw);
	      result = new String(stringBytes, "UTF8");
		} catch (Exception e) {
			 System.out.println("decrypt : " + e.getMessage());
		}
		return result;
	}
	
	public static  String encrypttext(String mensaje)  {
		String password = "password";  
		String salt = "77fd662d9e5a4871";
        TextEncryptor encryptor = Encryptors.text(password, salt);      
        return encryptor.encrypt(mensaje);
	}
	
	public static  String decrypttext(String mensaje)  {
		String password = "password";  
        String salt = "77fd662d9e5a4871";
       TextEncryptor encryptor = Encryptors.text(password, salt);
       return encryptor.decrypt(mensaje);
	}



   public static IvParameterSpec getivaes() throws Exception{
		return  new IvParameterSpec("77fd662d9e5a4871".getBytes());
   }
   
   public static String serialize(Object obj) throws IOException {
       ByteArrayOutputStream b = new ByteArrayOutputStream();
       ObjectOutputStream o = new ObjectOutputStream(b);
       o.writeObject(obj);
       return Base64.encodeBase64String(b.toByteArray());
   }
   public static Object deserialize(String bytes) throws IOException, ClassNotFoundException {
	   byte[] raw = Base64.decodeBase64(bytes);
       ByteArrayInputStream b = new ByteArrayInputStream(raw);
       ObjectInputStream o = new ObjectInputStream(b);
       return o.readObject();
   }
//   
//   public static ELoginSession getses(HttpSession ses){
//	   ELoginSession oELoginSession=null;
//	   if(ses.getAttribute("oELoginSession")!=null){
//				ESession oESession=(ESession) ses.getAttribute("oELoginSession");
//	    		oELoginSession=oESession.getoELoginSession();
//	    }
//	   return oELoginSession;
//   }
//   public static void setses(HttpSession ses,ELoginSession oELoginSession){
//	   if(ses.getAttribute("oELoginSession")!=null){
//				ESession oESession=(ESession) ses.getAttribute("oELoginSession");
//				oESession.setoELoginSession(oELoginSession);
//				ses.setAttribute("oELoginSession", oESession);
//	    }else{
//	    	System.out.println("creando la session");
//	    	ESession oESession= new ESession();
//	    	oESession.setoELoginSession(oELoginSession);
//	    	ses.setAttribute("oELoginSession", oESession);
//	    }
//   }
   
   public static boolean isliveses(HttpSession session)  {
	   return session.getAttribute("oELoginSession")!=null;
   }
   
   public static Key getkeyaesfile() throws Exception{
            String path = replaceSeparator(getProjectPath()+"util/main-aes-keystore.jck");
            System.out.println("PATH KEY: "+path);
		InputStream keystoreStream = new FileInputStream(path);
		KeyStore keystore = KeyStore.getInstance("JCEKS"); 
		keystore.load(keystoreStream, "mystorepass".toCharArray());  
		if (!keystore.containsAlias("jceksaes")) {  
		    throw new RuntimeException("Alias for key not found");  
		}
		Key key = keystore.getKey("jceksaes", "mykeypass".toCharArray());  
		return key;
  }
	
   public static Date StringtoDate(String Fecha){
	   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
       Date dfecha  = null;
		try {
			dfecha=sdf.parse(Fecha);
		} catch (ParseException e) {
			logger.error(e.getMessage());
			dfecha=null;
		}
		return dfecha;
   }
   public static int StrtoInt(String valor){
	  int res=0;
		try {
			res=Integer.parseInt(valor);
		} catch (Exception e) {
			logger.error(e.getMessage());
			res=0;
		}
		return res;
   }
 
   public static Map< Character, String > getestados(){
	   Map< Character, String > estados = new HashMap<Character, String>();  
	   estados.put('A', "Activo");
	   estados.put('X', "Inactivo");
	   return estados;
   }
		
	
	
    public static String urlapp(){
//    	return "http://www.jovenzuelo.com/zum/";
    	return "http://localhost:8080/zum/";
    }
    public static String getent(Object obj){
    	if(obj==null)return "";
    	int indx=obj.getClass().toString().lastIndexOf('.')+1;
		int len=obj.getClass().toString().length();
		return  obj.getClass().toString().substring(indx, len);
    }
    public static String getpasw(byte[] passw){
 	   String res="";
 		try {
 			res = new String(passw, "UTF8");
 		} catch (Exception e) {
 		}
 		return res;
    }
    
    private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                System.out.println("SERVER: " + aReply);
            }
        }
    }
    
    @SuppressWarnings("unused")
	private static void pconnectftp(){
    	String server = "54.85.101.234";
        int port = 21;
        String user = "ftpsiscomlineapunto";
        String pass = "$P&ftpsiscomlineapunto";
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            showServerReply(ftpClient);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                System.out.println("Operation failed. Server reply code: " + replyCode);
                return;
            }
            boolean success = ftpClient.login(user, pass);
            showServerReply(ftpClient);
            if (!success) {
                System.out.println("Could not login to the server");
                return;
            } else {
                System.out.println("LOGGED IN SERVER");
            }
        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }
    }

    @SuppressWarnings({ "unused", "rawtypes" })
	private static void pconnectsftp() throws Exception{
    	 JSch jsch = new JSch();
         Session session = null;
         try {
             session = jsch.getSession("ftpsiscomlineapunto", "54.85.101.234", 22);
             session.setConfig("StrictHostKeyChecking", "no");
             session.setPassword("$P&ftpsiscomlineapunto");
             String directory="/home/ftpsiscomlineapunto/backup";
             String directorylocal="/home/developeruser/Escritorio";
             session.connect();
             System.out.println("connect ok");
             Channel channel = session.openChannel("sftp");
             channel.connect();
             ChannelSftp sftp = (ChannelSftp) channel;
             sftp.cd(directory);
             //File f = new File(directorylocal+"mozilla.pdf");
             //sftp.put(new FileInputStream(f), f.getName());
             sftp.get("041114.backup", directorylocal+"/041114.backup");
     		 Vector files = sftp.ls("*");
     		 for (Object object : files) {
				System.out.println(object.toString());
			}
             System.out.printf("Found %d files in dir %s%n", files.size(), directory);
             sftp.exit();
             session.disconnect();
         } catch (JSchException e) {
             e.printStackTrace();  
         } 
    	
    }
    
    public static String executeCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
	public static void main(String[] args)  {
		//pconnectftp();
		//pconnectsftp();
		//pconnectsftp();
      //in mac oxs
//		String a[]={"Sabado","Domingo","Lunes","Martes","Miercoles","Jueves","Viernes"};
//		System.out.println("key"+encrypt("edgar"));
//		String val="DZxsYaAGpvGIMbU7EliE6Q==";
//		System.out.println("key"+decrypt(val));
//		GregorianCalendar cal=new GregorianCalendar();
//		cal.setTime(new Date());
//		Calendar calendario = Calendar.getInstance();
//		calendario.setTime(new Date());
//		//System.out.println("utlima dia semana"+a[cal.getMinimalDaysInFirstWeek()]);
//		System.out.println("utlima dia semana2:"+a[calendario.get(Calendar.DAY_OF_WEEK)]);
       
		//System.out.println(decrypt("zZzJIWCL3bid/QRTRMjCug=="));
	}
	
	public static String getRouteTemp(){
		return System.getProperty("java.io.tmpdir");
	}
	
	 public static Date StringtoDateandTime(String Fecha){
		   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	       Date dfecha  = null;
			try {
				dfecha=sdf.parse(Fecha);
			} catch (ParseException e) {
				logger.error(e.getMessage());
				dfecha=null;
			}
			return dfecha;
	   }
    public static String getProjectPath(){
        return getConfiguration("pathProject");
    }
    public static String getUploadPath(){
        return getConfiguration("pathUpload");
    }
    public static String getWebPath() {
        return getConfiguration("pathWeb");
    }
    public static String getAppDomain() {
        return getConfiguration("domain");
    }

    
    public static String replaceSeparator(String path){
        String separator = System.getProperty("file.separator");
        String newPath = path.replace("\\", separator);
        return newPath;
    }
    public static void createConfigurationFile(){
        Properties prop = new Properties();
        OutputStream output = null;
        String path = "";
        File child = new File(".././config.properties");
        if(!child.exists())
            try {
                path = child.getCanonicalPath();
                System.out.println("Configure PATH: "+path);
                output = new FileOutputStream(path);
                // set the properties value
                prop.setProperty("pathWeb", "/opt/wildfly/");
                prop.setProperty("pathProject", "/var/www/una/zum/");
                // save properties to project root folder
                prop.store(output, null);
            } catch (IOException io) {
                io.printStackTrace();
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
    }

    public static String getConfiguration(String property) {
        Properties prop = new Properties();
        InputStream input = null;
        String path = "";
        File child = new File("/opt/wildfly-8.1.0.Final/config.properties");
        String resultPath = "";
        try {
            path = child.getCanonicalPath();
            input = new FileInputStream(path);
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            resultPath = prop.getProperty(property);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return resultPath;
    }
    public static int diferenciaFechasEnAnos(Date initDate, Date FinDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(initDate);
        int year1 = calendar.get(Calendar.YEAR);
        int month1 = calendar.get(Calendar.MONTH);
        int day1 = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.setTime(FinDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int diffYears = (year - year1) + (month == month1 ? (day >= day1 ? 1 : 0) : month >= month1 ? 1 : 0);
        return diffYears;
    }
    public static String dateToFormatString(Date Fecha, String format) {
        String result = "";
        if (Fecha != null) {
            DateFormat fechaHora = new SimpleDateFormat(format);
            result = fechaHora.format(Fecha);
        }
        return result;
    }
}
